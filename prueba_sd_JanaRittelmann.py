# -*- coding: utf-8 -*-
'''
Author: Jana Rittelmann, october 9th, 2021

Prueba de nivel: Software Development Corify Care 
ELECTROCARDIOGRAM SIGNAL PROCESSING

This script provides signal pre-processing functions for ECG signals. 
In order to pre-process signals from a certain file, the name of this file 
must be indicated as the variable 'filename'.

The following functions are included: 
    
    - Recording spikes (artifacts of great amplitude) removal.
    - Low-pass filtering.
    - Powerline interference filtering.
    - Baseline wander and offset removal.
    - Detection of channels that are not connected.
    
The script has been tested with an exemplary data set (ecgConditioningExample.mat).
''' 

#%% Load libraries and import data from .mat-file

import numpy as np
import matplotlib.pyplot as plt 
from scipy import signal
from scipy.io import loadmat, savemat
from scipy.fftpack import fft


# Indicate file which contains ECG data
filename = 'ecgConditioningExample.mat'

# Import input data (ECG signals) and sampling frequency fs
ecg =  loadmat(filename, appendmat=False)['ecg'].T
fs = loadmat(filename, appendmat=False)['fs'][0][0]

number_of_channels = np.shape(ecg)[0] # Number of Channels/Electrodes 

time = range(np.shape(ecg[0])[0])/fs # x-axis for graphical visualisation


#%% Define filters that provide the necessary functions and corresponding variables

# Define variables
cutoff_LP = 40 # cut-off frequency for Low Pass Filter
cutoff_HP = 0.5 # cut-off frequency for High Pass Filter

powerline_lowerend = 48 # lower end of powerline frequency bandwidth
powerline_upperend = 52 # upper end of powerline frequency bandwidth

'''
Explanation why the cut-off frequency for the low pass filter is below the 
frequency for the powerline interference removal: 
    
    After analyzing the fourierspectrum of the given signal, it can be determined 
    that the cut-off frequency for the low pass filter should be at around 40 Hz, 
    as the frequencies that do not result of artifacts lie below 40 Hz. 

    It is assumed the program will apply to data collected in Europe, therefore a 
    powerline frequency of 50 Hz is used, applying a finite bandwidth of 4 Hz around 
    the centerfrequency (48-52 Hz) in order to portray a  more realistic picture.
    
    Therefore the powerline interference removal is implemented as a bandstop filter 
    at 50 Hz (48-52 Hz), although this is optional as the low pass filter in this case 
    also eliminates the powerline interference.
    
    The fourierspectrum can be displayed in the last part by uncommenting the code.
'''

# Define functions for filters

def spikes_removal(data, kernel_size=3):
    '''
    Recording spikes removal (artifacts of great amplitude): Median filter is used
    in order to remove impulse noise (spikes) that is not characteristic for the signal.
    
    Parameters
    ----------
    data : array
        Input ECG data.
    kernel_size : int, optional
        Defines size of the window used for the median filter. The default is 3.

    Returns
    -------
    data : array
        Output data, spikes removed.
        
    '''
    for i in range(len(data)):
        data[i] = signal.medfilt(data[i], kernel_size)
    return data


def bandpass(data, fs, order):
    '''
    Bandpass Filter: Low pass for removal of undesired high frequency noise signal 
    (EMG noise, additive white Gaussian noise...), high pass for removal of low 
    frequency noise (baseline wandering/offset, electrode contact noise, motion 
    artifacts...).

    Parameters
    ----------
    data : array
        Input ECG data.
    fs : int
        Sampling frequency.
    order : int
        Order of the filter.

    Returns
    -------
    data_bp : array
        Output data, ECG data filtered with bandpass filter.

    '''
    bp_filter = signal.butter(order, (cutoff_HP, cutoff_LP), btype = 'pass', fs = fs)
    data_bp   = signal.lfilter(*bp_filter, data)
    return data_bp


def bandstop(data, fs, order):
    '''
    Bandstop filter for powerline interference filtering.

    Parameters
    ----------
    data : array
        Input ECG data.
    fs : int
        Sampling frequency.
    order : int
        Order of the filter.

    Returns
    -------
    data_bs : array
        Output data, powerline interference removed.

    '''
    bs_filter = signal.butter(order, (powerline_lowerend, powerline_upperend), btype = 'stop', fs = fs)
    data_bs   = signal.lfilter(*bs_filter, data)
    return data_bs


def filtered(data, fs, order):
    '''
    Final filter that includes all the previously defined functions.
    The original ECG data is filtered by 3 different filters.

    Parameters
    ----------
    data : array
        Input ECG data.
    fs : int
        Sampling frequency.
    order : int
        Order of the filter.

    Returns
    -------
    data_3 : array
        Output data, original ECG data is filtered by a bandpass and bandstop filter
        (removes powerline interference, baseline wandering and offset, high 
        frequency noise), and spikes are removed.

    '''
    data_1 = spikes_removal(data)
    data_2 = bandpass(data_1, fs, order=4) 
    data_3 = bandstop(data_2, fs, order=4)
    return data_3


#%% Analyze data and display it:
# - Detect channels that are not connected and give a warning 
#   (Not connected means there is no signal detected, i. e. all entries = 0).
# - Display raw data for the different channels.

# Display raw data before filtering; each subplots contains one ECG channel.

fig, raw_plots = plt.subplots(number_of_channels, 1, sharex=True)
plt.subplots_adjust(hspace=3)
plt.xlabel('Time (s)')
        
for i in range(number_of_channels): 
    raw_plots[i].plot(time, ecg[i], linewidth=0.5)   
    raw_plots[i].set(ylabel='(mV)')
    if not np.any(ecg[i]):
        print('Warning: Channel ' + str(i+1) + ' is not connected!')
        raw_plots[i].set_title('Channel ' + str(i+1) + ': Not connected.')
    else:
        raw_plots[i].set_title('Channel ' + str(i+1))

plt.savefig('Raw_data_ECG.png')
plt.show()


#%% Filter the ECG data with the previously defined filters

filtered_ecg = filtered(ecg, fs = fs, order=4)


#%% Display filtered data 

fig, filtered_plots = plt.subplots(number_of_channels, 1, sharex=True)
plt.subplots_adjust(hspace=3)
plt.xlabel('Time (s)')
            
for i in range(number_of_channels): 
    filtered_plots[i].plot(time, filtered_ecg[i], linewidth=0.5)    
    filtered_plots[i].set(ylabel='(mV)')
    if not np.any(filtered_ecg[i]):
        filtered_plots[i].set_title('Channel ' + str(i+1) + ': Not connected.')
    else:
        filtered_plots[i].set_title('Channel ' + str(i+1))

plt.savefig('Filtered_data_ECG.png')
plt.show()


#%% Display an exemplary fraction of the filtered signal in Channel 2

fig, ax = plt.subplots()
ax.plot(time[1000:5000], filtered_ecg[1][1000:5000], linewidth=0.5)
ax.set(xlabel='Time(s)')
ax.set(ylabel='Amplitude (mV)')
ax.set_title('Exemplary fraction of filtered signal (Channel 2)')

plt.savefig('Exemplary fraction-Channel 2.png')
plt.show()


#%% Filtered data is saved in a .mat file

filtered_dict = {} 
filtered_dict['filtered_ecg'] = filtered_ecg.T
filtered_dict['fs'] = 1000 
savemat('ecgConditioningExample-filtered.mat', filtered_dict)


'''
#%% Fourierspectrum in order to analyze frequencies

fourierspectrum = fft(ecg)
frx = np.linspace(0, fs/2, num=int(len(fourierspectrum.T)/2))

for i in range(number_of_channels):
    fig, ax = plt.subplots()
    ax.plot(frx, np.abs(fourierspectrum)[i][:int(len(fourierspectrum.T)/2)], linewidth=0.5)
    ax.set_title('Channel ' + str(i+1))
    ax.set(xlabel='Frequency (Hz)')
    ax.set(ylabel='Amplitude')
'''

